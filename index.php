<?php 
/* 
* Template Name: Home Page 
* @package WordPress
*/

get_header();


if(have_rows('estrutura_da_pagina')){

	while (has_sub_field('estrutura_da_pagina')) {
		
		$conteudo = get_sub_field('tipo_de_conteudo');
		$conteudo = strtolower(str_replace(' ', '', $conteudo));
		$endereco = 'template-parts/' . $conteudo;

		if (	have_rows('sessao')		){
			while(	have_rows('sessao')		){
				the_row();
				$postagemid = get_sub_field($conteudo);
				wp_reset_postdata();
			}
		}
		include( get_template_directory() . '/' .  $endereco . '.php');
	}
}
get_footer(); 

 ?>