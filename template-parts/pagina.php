<link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/css/pagina.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:800" rel="stylesheet">

<div class="pagina">
	<div class="capa">
		<?php the_post_thumbnail(); ?>
	</div>
	<article>
		<div class="row conteudo">
			<div>
			<?php 
			   if( have_posts() ) {
			     while( have_posts() ) {
			     the_post();
			     the_content();
			   } } ?>
			   	
			</div>
		</div>
	</article>
	
</div>