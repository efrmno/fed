<?php 
	$impressao = new WP_Query(array(
		'p'=> $postagemid, 'post_type'=>$conteudo));

	if($impressao->have_posts()){
		$impressao->the_post();
 ?>
<div class="fluxos row">
	<h2><img src="<?php bloginfo("template_directory"); ?>/img/pattern.png"><?php the_title() ?></h2>
	<div class="row timeline">

		<!-- LOOP DA TIMELINE -->
		<?php 
		$contador = 0;
		if(have_rows('etapa')){ ?>
		<div id="linha"> <br> </div>

		<div id="fluxo" class="row align-justify">
			
			<?php 
			while (has_sub_field('etapa')) {
			$contador++; ?>
			<div class="etapa column small-3">
				<div class="numero">
					<?php echo $contador; ?>
				</div>
				<p>
					<?php the_sub_field('descricao_da_etapa') ?>
				</p>
			</div>

				<?php }  ?>
		</div>
		<?php  } ?>
	</div>
		<?php  

			if (have_rows('chamada')){
the_row(); 
		?>
	<div class="row align-center">
		<div class="column small-4">
		<center>
			<a href="<?php the_sub_field('endereco_do_botao') ?>" class="botao" style="background-color: <?php the_sub_field('cor_do_botao') ?>" >
				<?php the_sub_field('texto_do_botao') ?>
				
			</a>
			</center>
		</div>	
	</div>
	
		<?php 
			}  reset_rows(); ?>
</div>
<?php 
	} wp_reset_postdata();
 ?>