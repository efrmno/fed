<link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/css/pagina.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:800" rel="stylesheet">

<div class="pagina">
	<div class="capa">
		<?php the_post_thumbnail(); ?>
	</div>
	<article>
		<div class="row conteudo">
			<div class="medium-5 columns">
			<?php 
			   if( have_posts() ) {
			     while( have_posts() ) {
			     the_post();
			     the_content();
			   } } ?>
			   	
			</div>
			<div class="medium-7 columns">
				<strong>Passo 1 de 2</strong>
				<div class="row">
					<div class="medium-5 columns">
						<label><strong>* Nome:</strong></label>
						<p><input placeholder="First name..." oninput="this.className = ''"></p>
					</div>
					<div class="medium-7 columns">
						<label><strong>* Seu e-mail:</strong></label>
						<p><input placeholder="First name..." oninput="this.className = ''"></p>
					</div>
					<div class="medium-5 columns">
						<label><strong>* Telefone:</strong></label>
						<p><input placeholder="First name..." oninput="this.className = ''"></p>
					</div>
					<div class="medium-7 columns">
						<label><strong>* Campo de atuação profissional:</strong></label>
						<p><input placeholder="First name..." oninput="this.className = ''"></p>
					</div>
					<div class="medium-5 columns">
						<label><strong>* Cat. carteira motorista:</strong></label>
						<p><input placeholder="First name..." oninput="this.className = ''"></p>
					</div>
					<div class="medium-7 columns">
						<label><strong>* Gênero:</strong></label>
						<p><input placeholder="First name..." oninput="this.className = ''"></p>
					</div>
				</div>
			</div>
		</div>
	</article>
	
</div>