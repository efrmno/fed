<div id="callaction">
<?php 
	$impressao = new WP_Query(array(
		'p'=> $postagemid, 'post_type'=>$conteudo));

	if($impressao->have_posts()){
		$impressao->the_post();
 ?>
<div class="callactions">

	<div class="row align-justify ">

		<!-- GRUPO CHAMADA -->

		<?php  

			if (have_rows('chamada')){
				the_row(); 

		?>
		<div class="column small-5 chamada">
			<h2><?php the_sub_field('titulo') ?></h2>
			<div class="texto"><?php the_sub_field('resumo') ?></div>
		</div>
		<?php 
			}  reset_rows(); ?>

		<!-- GROUPO BOTÃO -->

		<?php  

			if (have_rows('botao_da_chamada')){
				the_row(); 

		?>
		<div class="column medium-5 botao">
			<a href="<?php the_sub_field('link_do_botao') ?>" style="background-color: <?php the_sub_field('cor_do_botao') ?>"><?php the_sub_field('chamada_do_botao') ?></a>
		</div>

		<?php 
			}  reset_rows(); ?>
	</div>

	<!-- GROPO GALERIA -->
		<?php  

			if (have_rows('galeria')){

		?>
	<div class="row acoes">
		<?php while (has_sub_field('galeria')) { ?>
		 <div class="column medium-5 acao">
		 	<div class="row">
			  <div class="medium-3 columns imagem"><img src="<?php the_sub_field('icone') ?>"></div>
			  <div class="medium-9 columns texto"><?php the_sub_field('chamada') ?><br><?php the_sub_field('legenda') ?></div>
			</div>
		</div>
	<?php } ?>
	</div>

	<?php 
			}  ?>
</div>
<?php 
	} wp_reset_postdata();
 ?>
</div>