<?php 
	$impressao = new WP_Query(array(
		'p'=> $postagemid, 'post_type'=>$conteudo));

	if($impressao->have_posts()){
		$impressao->the_post();
 ?>
<div class="destaques">
	<div class="row align-spaced">
		<div class="column small-4 texto">
			<?php the_content() ?>
		</div>
		<div class="column small-5">
			<img src="<?php the_post_thumbnail_url(); ?>">
		</div>
	</div>
</div>

<?php 
	} wp_reset_postdata();
 ?>