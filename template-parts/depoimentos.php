<center>
<div class="depoimentos">
	<div class="quadro ">
<?php 
	$impressao = new WP_Query(array(
		'p'=> $postagemid, 'post_type'=>$conteudo));

	if($impressao->have_posts()){
		$impressao->the_post();
		if(have_rows('depoimento')){
		while (has_sub_field('depoimento')) {
		
 ?>
		<div class="depoimento row medium-5">
			<i class="fas fa-quote-left"></i>
			<img src="<?php the_sub_field('foto'); ?>">
			<div class="texto">
				<?php the_sub_field('depoimento'); ?>
			</div>
			<div class="sobreodepoimento">
				<?php the_sub_field('sobre_o_depoimento'); ?>
			</div>
			<i class="fas fa-quote-right"></i>
		</div>
<?php } }
	} wp_reset_postdata();
 ?>
	</div>
	<div class="setas">
		<i class="fas fa-arrow-left"></i>
		<i class="fas fa-arrow-right"></i>
	</div>
</div>
</center>