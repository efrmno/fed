<?php 
	$impressao = new WP_Query(array(
		'p'=> $postagemid, 'post_type'=>$conteudo));

	if($impressao->have_posts()){
		$impressao->the_post();
 ?>
<div class="banneres">
	<img src="<?php the_field('imagem_para_desktop') ?>" alt="imagem destaque de pagina">
	<div class="row align-left">
	  <div class="column small-3 legendabanner" style="background-color: <?php the_field('cor_do_quadro_destaque') ?>">
	  	<?php the_field('quadro_destaque') ?>
	  	<a href="<?php the_field('link_chamada_quadro_destaque') ?>"><?php the_field('chamada_do_quadro_destaque') ?></a>
	  </div>
	</div>
</div>

<?php 
	} wp_reset_postdata();
 ?>