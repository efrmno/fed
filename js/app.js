$(document).foundation();

window.sr = ScrollReveal();

sr.reveal('.legendabanner p')
sr.reveal('#navbar .opcoes ul li', 50);
sr.reveal('.acoes .acao .imagem', 50);
sr.reveal('.acoes .acao .texto', 100);
sr.reveal('.fluxos .etapa p', 125);
sr.reveal('.destaques .column', 90);

$('.depoimentos .quadro').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    prevArrow: $('.setas .fa-arrow-left'),
    nextArrow: $('.setas .fa-arrow-right')
});