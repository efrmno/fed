<footer>
	<div class="row">
			<strong>Front-End Challenge</strong>
	</div>
	<div class="row align-justify">
		<div class="medium-6 columns">
			<div class="row">
				<div class="small column opcoes">
					<ul>
						<li>Inicial</li>
						<li>Cursos</li>
						<li>Blog</li>
						<li>Contato</li>
					</ul>
				</div>					
				<div class="small column">
					<strong>ALUNOS</strong>
					<ul>
						<li>Crie uma conta</li>
						<li>Acesse os cursos</li>
						<li>Precisa de ajuda?</li>
					</ul>
				</div>	
			</div>
			
		</div>
		<div class="medium-4 columns end botoes">
			<div class="row align-justify">
				<a href="" class=" cursos">Veja os cursos</a>
				<a href="" class=" contato">Entre em contato</a>
			</div>
			<div class="row facebook">
				<a href=""><img src="<?php bloginfo("template_directory"); ?>/img/fb.png"></a>
			</div>
			<div class="row Copyright">
				<p>Copyright ₢ 2018</p>
			</div>
		</div>
	</div>
	<div class="row">
		<ul>
			<li>Termos de uso</li>
			<li>Política de Privacidade</li>
		</ul>
	</div>
	<div class="row  align-right">
		  <a href=""><img src="<?php bloginfo("template_directory"); ?>/img/geo.png"></a>
		
	</div>
</footer>
<script src="<?php bloginfo("template_directory"); ?>/js/app.js"></script>
<?php wp_footer() ?>
  </body>
</html>
