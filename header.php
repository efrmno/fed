<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>

    <!-- CSS -->
    <link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/css/foundation.min.css">
    <link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/style.css">
    <link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/css/fonts.css">
    <link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/slick/slick.css">
    <link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/af/css/fontawesome-all.min.css">



    <!-- SEO -->
    <title> <?php bloginfo('name') ?> </title>
    
    <meta property="og:locale" content="pt_BR">
    <meta property="og:url" content="http://www.meusite.com.br/ola-mundo">
    <meta property="og:title" content=" <?php bloginfo('name') ?> ">
    <meta property="og:site_name" content=" <?php bloginfo('name') ?> ">

    <meta property="og:description" content="Minha boa descrição para intrigar os usuários.">

    <meta property="og:image" content="www.meusite.com.br/imagem.jpg">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="800"> 
    <meta property="og:image:height" content="600">

    <!-- SCRIPTS -->
    <?php wp_head(); ?>
    <script src="<?php bloginfo("template_directory"); ?>/js/jquery.js"></script>
    <script src="<?php bloginfo("template_directory"); ?>/js/what-input.js"></script>
    <script src="<?php bloginfo("template_directory"); ?>/js/foundation.js"></script>
    <script src="<?php bloginfo("template_directory"); ?>/js/scrollreveal.min.js"></script>
    <script src="<?php bloginfo("template_directory"); ?>/slick/slick.js"></script>


  </head>
  <body>

<div id="navbar">
    <div class="row">
        <div class="medium-2 columns logo">
            <ul>
                <li><a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo("template_directory"); ?>/img/logo.png"></a></li>
            </ul>
            
        </div>
        <div class="medium-5 columns opcoes">
            <ul>
                <li><a href="">Cursos</a></li>
                <li><a href="">Blog</a></li>
                <li><a href="">Contato</a></li>
                <li><a href=""><img src="<?php bloginfo("template_directory"); ?>/img/fb.png"></a></li>
            </ul>
        </div>
        <div class="medium-5 columns end painel">
            <ul>
                <li><a href="">Área do aluno</a></li>
                <li class="botao"><a href="">INSCREVA-SE</a></li>
            </ul>
        </div>
    </div>
</div>